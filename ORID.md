## O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?

1. Conduct a Code Review and show case to discuss the code.

2. Learn the observer pattern and try to use the observer pattern in your code.

3. Learn unit testing and understand the role and meaning of unit testing.

4. Learn Test Driven development (TDD), which includes learning What is TDD and How to TDD, followed by several code exercises on TDD.

## R (Reflective): Please use one word to express your feelings about today's class.

    Thoughtful.

## I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?

   The teacher took us step by step, so that I could understand these contents from shallow to deep, and practice them through code, which was very rewarding.

## D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?

    We can focus on TDD in the software development process, by writing test samples to determine the perfection of the function.
