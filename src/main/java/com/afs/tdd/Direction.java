package com.afs.tdd;

public enum Direction {
    SOUTH, WEST, EAST, NORTH
}
