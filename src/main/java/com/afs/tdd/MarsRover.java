package com.afs.tdd;

import java.util.List;

public class MarsRover {

    private Location location;

    public MarsRover(Location location) {
        this.location = location;
    }

    public void executeCommand(Command command) {
        switch (command) {
            case MOVE:
                move();
                break;
            case TURN_LEFT:
                turnLeft();
                break;
            case TURN_RIGHT:
                turnRight();
                break;
        }
    }

    public void executeCommand(List<Command> command) {
        command.forEach(this::executeCommand);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    private void move() {
        switch (location.getDirection()) {
            case NORTH:
                updateLocationY(1);
                break;
            case SOUTH:
                updateLocationY(-1);
                break;
            case WEST:
                updateLocationX(-1);
                break;
            case EAST:
                updateLocationX(1);
                break;
        }
    }

    private void turnLeft() {
        switch (location.getDirection()) {
            case NORTH:
                changeDirection(Direction.WEST);
                break;
            case SOUTH:
                changeDirection(Direction.EAST);
                break;
            case WEST:
                changeDirection(Direction.SOUTH);
                break;
            case EAST:
                changeDirection(Direction.NORTH);
                break;
        }
    }

    private void turnRight() {
        switch (location.getDirection()) {
            case NORTH:
                changeDirection(Direction.EAST);
                break;
            case SOUTH:
                changeDirection(Direction.WEST);
                break;
            case WEST:
                changeDirection(Direction.NORTH);
                break;
            case EAST:
                changeDirection(Direction.SOUTH);
                break;
        }
    }

    private void changeDirection(Direction direction) {
        location.setDirection(direction);
    }

    private void updateLocationY(int step){
        location.setLocationY(location.getLocationY() + step);
    }

    private void updateLocationX(int step){
        location.setLocationX(location.getLocationX() + step);
    }

}
