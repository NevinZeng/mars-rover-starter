package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MarsRoverTest {

    @Test
    void should_only_plus_location_y_1_when_executeCommand_move_given_location_north() {
        // given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 1, Direction.NORTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_only_decrease_location_y_1_when_executeCommand_move_given_location_south() {
        // given
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, -1, Direction.SOUTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_only_decrease_location_x_1_when_executeCommand_move_given_location_west() {
        // given
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(-1, 0, Direction.WEST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_only_plus_location_x_1_when_executeCommand_move_given_location_east() {
        // given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MOVE);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(1, 0, Direction.EAST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_west_when_executeCommand_turnLeft_given_location_north() {
        // given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_LEFT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.WEST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_east_when_executeCommand_turnLeft_given_location_south() {
        // given
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_LEFT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.EAST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_south_when_executeCommand_turnLeft_given_location_west() {
        // given
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_LEFT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.SOUTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_north_when_executeCommand_turnLeft_given_location_east() {
        // given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_LEFT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.NORTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_east_when_executeCommand_turnRight_given_location_north() {
        // given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_RIGHT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.EAST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_west_when_executeCommand_turnRight_given_location_south() {
        // given
        Location location = new Location(0, 0, Direction.SOUTH);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_RIGHT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.WEST);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_north_when_executeCommand_turnRight_given_location_west() {
        // given
        Location location = new Location(0, 0, Direction.WEST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_RIGHT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.NORTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_south_when_executeCommand_turnRight_given_location_east() {
        // given
        Location location = new Location(0, 0, Direction.EAST);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TURN_RIGHT);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(0, 0, Direction.SOUTH);
        assertEquals(expectedLocation, currentLocation);
    }

    @Test
    void should_change_direction_to_north_and_decrease_location_x_2_when_executeCommand_turnLeft_move_move_turnRight_given_location_north() {
        // given
        Location location = new Location(0, 0, Direction.NORTH);
        MarsRover marsRover = new MarsRover(location);
        List<Command> commandList = List.of(Command.TURN_LEFT, Command.MOVE, Command.MOVE, Command.TURN_RIGHT);
        // when
        marsRover.executeCommand(commandList);

        // then
        Location currentLocation = marsRover.getLocation();
        Location expectedLocation = new Location(-2, 0, Direction.NORTH);
        assertEquals(expectedLocation, currentLocation);
    }

    private void assertEquals(Location expectedLocation, Location actualLocation){
        Assertions.assertEquals(expectedLocation.getLocationX(), actualLocation.getLocationX());
        Assertions.assertEquals(expectedLocation.getLocationY(), actualLocation.getLocationY());
        Assertions.assertEquals(expectedLocation.getDirection(), actualLocation.getDirection());
    }
}
